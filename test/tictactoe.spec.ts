import { expect } from "chai";
import { TicTacToe } from "../src/tictactoe";


describe('TicTacToe', () => {

    it('should be able to play a move', () =>{

        const ticTacToe = new TicTacToe();

        ticTacToe.play(0, 0);

    });

    it('Display the current grid state', ()=> {
        const ticTacToe = new TicTacToe();

        const grid = ticTacToe.grid();

        expect(grid).to.deep.equal([
            [null,null,null], 
            [null,null,null], 
            [null,null,null]
        ]);
    });

    it('Should put symbol at given coordinates on play', () => {

        const ticTacToe = new TicTacToe();

        ticTacToe.play(0, 0);
        const grid = ticTacToe.grid();

        expect(grid).to.deep.equal([
            ["X",null,null], 
            [null,null,null], 
            [null,null,null]
        ]);

    })

    it('Should alternate symbols when played consecutively', () => {

        const ticTacToe = new TicTacToe();

        ticTacToe.play(0, 0);
        ticTacToe.play(0, 1);
        const grid = ticTacToe.grid();

        expect(grid).to.deep.equal([
            ["X","O",null], 
            [null,null,null], 
            [null,null,null]
        ]);

    })



});