

export class TicTacToe {
    private state:String[][];

    constructor() {
        this.state = [
            [null, null, null],
            [null, null, null],
            [null, null, null]
        ];
    }

    grid() {

        return this.state;

    }


    play(x: number, y: number) {
        this.state[x][y] = 'X';
    }
}